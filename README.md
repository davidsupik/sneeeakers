How to start app:

### How to start app

    npm install --legacy-peer-deps
    npm start
    
This is a small primer on how to use GLTF models on the web, specifically how to use them as dynamic assets.

### Where to find project

Project repository:
https://gitlab.com/davidsupik/sneeeakers

Live demo:
https://sneeeeakers.netlify.app


### How to compress assets and turn them into JSX components

1. `npx gltf-pipeline -i model.gltf -o model.glb --draco.compressionLevel=7`
2. `npx gltfjsx model.glb`

gltfjsx turns gltf model int react components
https://github.com/pmndrs/gltfjsx



### How to include them in project

1. Set up [react-three-fiber](https://github.com/pmndrs/react-three-fiber)
3. Put `model.glb` into `/public`
3. Put `Model.js` (the output of [gltfjsx](https://github.com/pmndrs/react-three-fiber)) anywhere inside `/src`


App is urnning on Tree js - JavaScript 3D library
https://threejs.org